import 'package:flutter/material.dart';

import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:peliculas/src/models/pelicula_model.dart';

class CardSwiper extends StatelessWidget {

  final List<Pelicula> peliculas;

  CardSwiper( { @required this.peliculas } );

  @override
  Widget build(BuildContext context) {

    // informacion sobre ancho y alto del dispositivo
    final _screenSize = MediaQuery.of(context).size;

    return Container(
      padding: EdgeInsets.only(top: 10.0),
      child: Swiper(
        itemBuilder: (BuildContext context,int index){       
          
          peliculas[index].uniqueId = '${ peliculas[index].id }-tarjeta';
                     // imagen se adapte a las dimensaiones que tiene
          return Hero(
            tag: peliculas[index].uniqueId,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(20.0),
              child: GestureDetector(
                child: FadeInImage(
                  placeholder: AssetImage('assets/img/no-image.jpg'), 
                  image: NetworkImage( peliculas[index].getPosterImg() ),
                  fit: BoxFit.cover,
                ),
                onTap: () {
                  print('Tap on card movie');
                  Navigator.pushNamed(context,'detalle',arguments:peliculas[index]);
                },
              ),
            ),
          );
        },
        itemCount: peliculas.length,
        // pagination: new SwiperPagination(), PUNTITOS ABAJO 
        // control: new SwiperControl(),  FLECHAS A LOS COSTADOS
        itemWidth:  _screenSize.width * 0.7,
        itemHeight: _screenSize.height * 0.5,
        layout: SwiperLayout.STACK,
      ),
    );
  }
}