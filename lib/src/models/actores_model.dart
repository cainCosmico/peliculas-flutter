class Cast {
  List<Actor> actores = new List();

  Cast.fromJsonList( List<dynamic> jsonList ) {
    if ( jsonList == null ) return;

    jsonList.forEach( (item) {
      final actor = Actor.fromJsonMap(item);
      actores.add(actor);
    });
  }
}

class Actor {
  int castId;
  String character;
  String creditId;
  int gender;
  int id;
  String name;
  int order;
  String profilePath;

  final String imagePath = 'https://developers.themoviedb.org/3/getting-started/images';

  Actor({
    this.castId,
    this.character,
    this.creditId,
    this.gender,
    this.id,
    this.name,
    this.order,
    this.profilePath,
  });

  Actor.fromJsonMap( Map<String, dynamic> json ) {
    this.castId      = json['cast_id'];
    this.character   = json['character'];
    this.creditId    = json['credit_id'];
    this.gender      = json['gender'];
    this.id          = json['id'];
    this.name        = json['name'];
    this.order       = json['order'];
    this.profilePath = json['profile_path'];
  }

  getFoto() {
    if ( profilePath == null ) {
      return 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMSEhUTEhIVFRUXFhUSGBYXFhAYFRgWFxIWFhUVFRYYHSggGBolGxcVITEhJSkrLi4uGB8zODMtNygtLysBCgoKBQUFDgUFDisZExkrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrK//AABEIAOEA4QMBIgACEQEDEQH/xAAcAAEAAgMBAQEAAAAAAAAAAAAAAQIDBgcFBAj/xABEEAACAQIBCAUIBggHAQAAAAAAAQIDEQQFBhIhMUFRcQciYYGREzJSU5KhsdEWI2JygsE0QnOis8Ph8BQkM0OywvEV/8QAFAEBAAAAAAAAAAAAAAAAAAAAAP/EABQRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/AO4gAARGVyk5FoAWAAAAAADHVqJJt7Em2+xK7AyA8DNrOKOKTUrU6qlL6tvW4X6sl6Wqydt/cexiMZTprSqTjBcZSjFeLAzg1rHZ9YOnqVSVR8KcW/3naPvPDxPSWv8AbwzfbOaXuin8QOgg5i+kmvuo0rc6nzPrwnSXr+tw+rjCev2ZL8wOhg8nIuceHxWqlU621wl1Zru381dHrACIyvrKSlctDYBYAAAAAAKNgXBQsmBIAAGOUi8lqKwiAhEuAAAAAAAVqRumk7XTV+HackypnFitB4bEKM3CaTk1aalBtNXWp6t9t995105j0pVaEK0NFPy0o3qWto6GtQ0l6erU+C17gNcqyjNKc1aKvZX1t93I+Cp1nfw36uZWVXStwWpLgQgLKBayMdyI69msDKHEpKDW1Nc0yFIC0ZOLTi2mndNNpp7mmtjOm5l51vEJUK7+tS6stnlEv+y7NpzNSLvENSUodWSaknHVaS2OPCwHeoRLnlZs5WWKw8KurS82aW6a87ue1djR6oAAAAAAKRLlXECCyQSJAAAAAAAAAAAAAABwzOTF+XxVao9d6kkvuxejD91I7diqmjCUvRjKXgmz8/x2AWoYfSkoxXWk1Fc27L3nWYZnYNWvRu0kr6dbW7bbKRpfR/k/yuKU2urSWn+J6oL4v8J1EDzMPm9hYebh6ffFSfjK56NOmo6opLkkvgWJAM8zKGb+GrL6yjC/pRWjL2o2fiemQBzDOfNCeHTqU26lJbfTgvtW2rtXet5q53dq+p61sOQ525KWGxMoRXUklUh2Rd+r3NNcrAbF0V461SrRb1Siqq5xajLxUl7J0k45mDV0cdR+1pxffTk/ikdjAAAAAAAAAAAAAAAAAAAAAAAAA+bKUb0ai405rxizgiP0G1c4JQwblXWHXn+U8jyanotvlZvuA6R0e5P8nhdNrrVZOf4V1Yfm/wARs5ShRUIxhFWjFKKXBJWS8EXAEggCSASBBpHSfhrwo1eEpU3+JaS/4vxN3PHzvwDr4SpCKvNJVIre3B3su1q67wOeZkr/AD2H+9L+HJnaDjnRuvKY+m1sjCpP9zQ+M0djAAAAAAAAAAAAAAABWTAsCijzLRYEgAAAAMdadu81uhmxRji5YtaWm7vR1aCnJWlNar3evxZsOKWx9vxMIAiLuQ2WiAAAAAACSCAPKybm/RoYipiKV4yqRcXHVoK8lKTirartLVsPeoVL3TPmsZcLtb5ID6QAAAAAANgGyIu5jlK5kitQEgAAURchoCrLRQSJAAAAAAK1Ipppnn1qij57SV1G71JtuyWve3qsejJXPHzpwLq4StFedo6cbbdKDU427bxQH1xRJ5ubuVVicPCr+tbRmuE153jtXY0ekABJ8WJyth6btOvSi+DnBPwuB9gPlw2UqNR2p1qc3wjODfgmfWBBEST4MvZSWGoTqvalaK4zeqK8dvYmB9sKik2otNp6LSex2v1nu2o+yjT0Vb+7mudH1GX+F8pNtyrVJ1Xfg2orx0b95swAAAAAAbMUncvNXEIgIRLAAAAAAAAAAAAAAAAAAcxoYr/5mPqUpaqFRqXYoybcJpfZ1xfJ8EdAhJNJpppq6a1pp7GmeF0g5BeIo+Upq9WldpLbKH60e171ya3nPMmZyYmhDQpVbR3JqMlG/o3WrlsA3HO7KVWrWhgcNK05a6kk7WTV9Fta0lHrPiml2P6MDmJhYRXlFKrLe3KUVfsjBqy5tms5gYi+OcpyvKcKmtvW5Nxk++ykdPA1PKOYeHnH6pypS3dZyjftUtfgymZuVasak8HiXepC+g27tpbY336rSXZfgbectz4xWjj3KnLRlBU+stqmo3+DSA6i2c4y9jXlHF08NRf1aloqS2N/7lXkop2/qeVlLOrFV6fk5zSi1aWjFRcvvPh2KyNy6NchunTeJmutUVoLhT26X4nbuS4gbnh6EacIwgrRjFRS4JKyXgZAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHNs+Mz3GUsRh43g+tOmtsXvlBb4vet3LZ0kpW2AcCw9eUJRnCTjKLUk1ua2G+5N6QIaKWIpSUvSp2cX2uLacfeevlnNHD4huVnTm9elC1m+Mo7H7n2mr4ro+rp9SrTmvtacH4Wa94Hp5Sz/p6LWHpyc90ppKK7bJty5ajn9arKcnKTblJuTb2tt3bZtFLMDFN65UYrjpTfuUTYskZjUabUqz8tJbmrU++N3pd7t2Aa/mXmm8TJVaytQTuk9tR8F9ji9+xcV1aKtqWwxYZav74GYAAAAAAAAAAAAAAAAAAABWErlJTuXggLAAAAAAB8OOyrRpO1SrGL9G95PlFawPuMdd6jU8qZ7Qp6qVKUnulJqMe612++xGamXJ4idVVZJydpwS1JR81pLdZ28QNkAAAAAZ8O9pmNRzyytKhCn5OWjUc9Jfdine63q7ij5sm5/p2Vek09mlTd17L2eLA3ZsRd0ebg8s0K1tCrFt7It2k/wuzZ6UUBIAAAAAAUbvy+IFwUtwLJgSAABilK5kkisY8QEIlwAABq2d2dP+H+qo2dVrW9qgns1b5dne+0Pbynlejh1erUUeC2yfKK1s1PG9ISd1Qov71R2/dj80aVWqynJynJyk9bk2233s+ePnMD2soZyYqt51aSXow6i5atb72zzozaakt2p8+3uMRMXYD7p1U0pzS36Mfi2YcJjZ0qkakHaUdnC2+NuG0+eUrkAdOyLnFSxCSuoVN8JPf9h/rL3nsnGD7cPlfEQVo1qiXDSk13J6kB1o8zK+XKOHT05XnuhGzk+fortZzqtlrETVpV6luClJLvtY+AD7Mq5RniKjqT2vUktkYrZFGGi0turwerh/TeYi1So5a3/72sCJPdu7fzPRyfl/E0fMrSt6MutHklK9u6x5gA3bBdITX+vRv9qm7P2ZfM2vJOXaGJX1VROVr6D1TX4Xt5q6ONVtyM0JNNNNprWmm00+Ka2AdxBpmaGdjqNUMQ+u9UJ+l9mX2uD389u5gCkS5DQFSyQSJAAAAAAAAA8/L2UVh6E6u9K0Vxk9UV4nH6tSU5OUm3Jtybe1tvWze+kyu1CjT3SlOfsRSX/M0RAQYmusmZrGKpu5gWAJAgE2FgBBNibXAqSTYiwEAmwsBAJsQBWWuSMiMa87uMq18wK7DrOaeVXicPGUvPj1J9rSXW700+9nKLG4dGld+UrQ3OMZ98ZW/wCy8AOgAAAAAAAAAAAAANE6TtuH5Vv5RpKN26TtuH5Vv5RpIE7THJfH8y5DYBEkIkAAABIRAEvXzIBO0CAAAIZJDAqltMiKIsgJ28/ibT0b/pM/2T/iQNVNr6N3/mZ/sn/EgB0YAAAAAAAAAq2BYFNEtFgaL0nbcPyrfyjSUbt0nbcPyrfyjSEBJjqPZzRkMNR60BluLkACy1gz04KKu/Ffl7+djBUld3/v+nIA2LlQBa4uVAFmyLkGalB3SXnP3Ljz/vaBiuLmatGUXaWu/v7+JilG3LagMaet9xkRiv1u4yICTaujf9Jn+yf8SBqptXRv+kz/AGT/AIkAOjgFZSsAnLxJRjSuZQAAAFC5DQFSyQSJA1TPnItbEuj5GKloeU0ryivO0Lbfus1f6G4z1a9un8zqYA5Z9DcZ6te3T+ZilmVjb38lH26fzOsADlf0Nxnq4+3T+Zko5o4tP/TXHVUp962nT5IiMbAcwlmdjN1NW2+fT8dpX6G4z1a9un8zqYA5Z9DcZ6te3T+Y+huM9Wvbp/M6mAOWfQ3GerXt0/mPobjPVr26fzOpgDln0Nxnq17dP5mSOaWMTTVON7L9enws9508hoDnDzWxTWk6UXLYlpwsu3afLLM7GPbTXt0/mdRSJA5Q8y8be/ko+3T+Zk+huM9Wvbp/M6mAOWfQ3GerXt0/mbBmVkCvh60p1YKMXTcVaUXr04vc+xm5gCspWKJXMko3CQBKxIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAf/2Q==';
    } else {
      return imagePath + profilePath;
    }
  }
}


